package cn.lds.demo.bean.protocol.xmlreport;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@XmlRootElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
@XmlAccessorType(XmlAccessType.NONE)
public class DsSignature {

    @XmlElement(name = "SignedInfo", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private DsSignedInfo dsSignedInfo;

    @XmlElement(name = "SignatureValue", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private String dsSignatureValue;
}