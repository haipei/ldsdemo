package cn.lds.demo.bean.protocol.xmlreport;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@XmlRootElement(name = "CanonicalizationMethod", namespace = "http://www.w3.org/2000/09/xmldsig#")
@XmlAccessorType(XmlAccessType.NONE)
public class DsCanonicalizationMethod {
    @XmlAttribute(name = "Algorithm")
    private String algorithm;
}
