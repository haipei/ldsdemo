package cn.lds.demo.bean.protocol.enums;

import cn.lds.demo.bean.protocol.Payload;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum PacketType {
    /**
     * 统计报文相关(非协议定义，内部实现使用)
     */
    STDATAREPORT((byte) 0xA5), //StDataReportRequest 统计报文接收
    STDATARESPONSE((byte) 0xA6), //StDateReportResponse 统计报文返回

    UNKNOWN((byte) -1); //未知

    private final byte value;

    private PacketType(final byte value) {
        this.value = value;
    }

    private static final Map<Integer, PacketType> values = new HashMap<Integer, PacketType>();;

    static {
        for (final PacketType myEnum : PacketType.values()) {
            PacketType.values.put(Integer.valueOf(myEnum.getValue()), myEnum);
        }
    }

    @JsonCreator
    public static PacketType getPacketType(final int value) {
        PacketType result = PacketType.values.get(value);
        if (result == null) {
            result = PacketType.UNKNOWN;
        }
        return result;
        //
        //        for (final PacketType mtype : PacketType.values()) {
        //            if (mtype.getValue() == packetType) {
        //                return mtype;
        //            }
        //        }
        //        return PacketType.UNKNOWN;
    }

    @JsonValue
    public byte getValue() {
        return value;
    }

    @SuppressWarnings("unchecked")
    @JsonIgnore
    private Class<? extends Payload> getBodyClazz() {
        final String payloadClassName = Payload.class.getPackage().getName() + ".payload." + name();
        try {
            return (Class<? extends Payload>) PacketType.class.getClassLoader().loadClass(payloadClassName);
        } catch (final ClassNotFoundException e) {// 编码bug类错误
            throw new IllegalStateException("类找不到: " + payloadClassName, e);
        }
    }

    @JsonIgnore
    @Override
    public String toString() {
        final String str = Integer.toHexString(getValue()).toUpperCase();
        return str.length() < 2 ? "0" + str : str.substring(str.length() - 2);
    }

    @JsonIgnore
    public Payload newPayload() {
        try {
            return getBodyClazz().newInstance();
        } catch (final InstantiationException e) {
            throw new IllegalStateException("实例化类错误。", e);
        } catch (final IllegalAccessException e) {
            throw new IllegalStateException("实例化类权限错误。", e);
        }
    }

    @JsonIgnore
    public static boolean needForwading(final byte type) {
        switch (type) {
            case (byte) 0x01: //认证请求数据结构
            case (byte) 0x03: //心跳数据结构
            case (byte) 0x05: //通知确认数据结构
            case (byte) 0x11: //退票请求数据结构
            case (byte) 0x12: //补登请求数据结构
            case (byte) 0x15: //票务受理操作确认数据结构
            case (byte) 0x1C: //售票原始数据上报数据结构
            case (byte) 0x1E: //售票原始数据结构
            case (byte) 0x21: //票房统计上报数据结构
                return true;
        }

        return false;
    }

    @JsonIgnore
    public static boolean cannotAnswerInForwadingMode(final byte type) {
        switch (type) {
            case (byte) 0x02: //认证请求数据结构
            case (byte) 0x04: //通知确认数据结构
            case (byte) 0x13: //票务操作确认数据结构
            case (byte) 0x14: //票务受理操作数据结构
            case (byte) 0x1D: //售票原始数据查询数据结构
            case (byte) 0x1F: //售票原始数据确认数据结构
            case (byte) 0x20: //票房统计上报数据查询数据结构
            case (byte) 0x22: //票房统计上报数据确认数据结构
                return true;
        }

        return false;
    }
}
