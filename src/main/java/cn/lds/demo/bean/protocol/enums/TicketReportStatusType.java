package cn.lds.demo.bean.protocol.enums;

public enum TicketReportStatusType {
    Normal /** 正常上报 */, Past /** 过时补报 */
}
