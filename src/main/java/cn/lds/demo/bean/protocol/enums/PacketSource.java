package cn.lds.demo.bean.protocol.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum PacketSource {
    CINEMA((byte) 1),
    MIDPLATFORM((byte) 2),
    NETSALE((byte) 3),
    CORE((byte) 4),
    COREREPORT((byte) 5),
    CLOUDTICEKETSELLER((byte)6),
    UNKNOWN((byte) -1);

    private final byte value;

    private PacketSource(final byte value) {
        this.value = value;
    }

    @JsonCreator
    public static PacketSource getReceiveDataType(final int receiveDataType) {
        for (final PacketSource mtype : PacketSource.values()) {
            if (mtype.getValue() == receiveDataType) {
                return mtype;
            }
        }
        return PacketSource.UNKNOWN;
    }

    @JsonValue
    public byte getValue() {
        return this.value;
    }
}
