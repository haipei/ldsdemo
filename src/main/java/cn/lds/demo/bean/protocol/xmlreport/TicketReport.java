package cn.lds.demo.bean.protocol.xmlreport;

import cn.lds.demo.bean.protocol.enums.TicketReportStatusType;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "TicketReport")
@XmlAccessorType(XmlAccessType.NONE)
public class TicketReport {
    @XmlAttribute(name = "Id")
    private String id;

    @XmlAttribute(name = "Status")
    private TicketReportStatusType status;

    @XmlElement(name = "Cinema")
    private List<Cinema> cinemaList = new ArrayList<Cinema>();
}
