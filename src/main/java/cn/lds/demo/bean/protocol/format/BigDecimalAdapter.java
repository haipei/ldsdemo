package cn.lds.demo.bean.protocol.format;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class BigDecimalAdapter extends XmlAdapter<String, BigDecimal> {

    private final DecimalFormat df = new DecimalFormat("0.0000");
    private final RoundingMode roundingMode = RoundingMode.HALF_UP;

    @Override
    public BigDecimal unmarshal(final String money) throws Exception {
        final BigDecimal moneyDev = new BigDecimal(money).setScale(4,
                this.roundingMode);
        return moneyDev;
    }

    @Override
    public String marshal(final BigDecimal money) throws Exception {
        this.df.setRoundingMode(this.roundingMode);
        return this.df.format(money);
    }
}
