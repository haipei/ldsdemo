package cn.lds.demo.bean.protocol.xmlreport;

import cn.lds.demo.bean.protocol.enums.CinemaStatusType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "Cinema")
@XmlAccessorType(XmlAccessType.NONE)
public class Cinema {
    @Length(max = 8, min = 8)
    @XmlAttribute(name = "Code")
    private String code;

    @XmlAttribute(name = "Status")
    private CinemaStatusType status;

    @XmlElement(name = "Session")
    private List<Session> sessionList = new ArrayList<Session>();
}
