package cn.lds.demo.bean.protocol;

import cn.lds.demo.bean.protocol.enums.PacketSource;
import cn.lds.demo.bean.protocol.enums.PacketType;
import cn.lds.demo.bean.protocol.xmlreport.StDataReportRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;


/**
 * 13规范接收的消息体接口定义
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "typeName")
@JsonSubTypes({
        @JsonSubTypes.Type(value = StDataReportRequest.class, name = "StDataReportRequest")
})
public interface Payload extends Serializable {
    /**
     * 消息体的协议类型
     *
     * @return
     */
    PacketType getPacketType();

    /**
     * 消息体的协议类型说明
     *
     * @return
     */
    String getDiscription();

    /**
     * 数据的来源/渠道
     *
     * @return
     */
    PacketSource getPacketSource();

}
