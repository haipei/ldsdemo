package cn.lds.demo.bean.protocol.xmlreport;

import cn.lds.demo.bean.protocol.format.BigDecimalAdapter;
import cn.lds.demo.bean.protocol.format.DateAdapter;
import cn.lds.demo.bean.protocol.format.DateTimeAdapter;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@XmlRootElement(name = "Session")
@XmlAccessorType(XmlAccessType.NONE)
public class Session {
    @XmlJavaTypeAdapter(value = DateAdapter.class)
    @XmlElement(name = "BusinessDate")
    private Date businessDate;
    @Length(max = 16, min = 1)
    @XmlElement(name = "ScreenCode")
    private String screenCode;
    @Length(max = 12, min = 12)
    @XmlElement(name = "FilmCode")
    private String filmCode;
    @Length(max = 16, min = 1)
    @XmlElement(name = "SessionCode")
    private String sessionCode;
    @XmlJavaTypeAdapter(value = DateTimeAdapter.class)
    @XmlElement(name = "SessionDatetime")
    private Date sessionDatetime;
    @Range(min = 0, max = 9999)
    @XmlElement(name = "LocalSalesCount")
    private int localSalesCount;
    @Range(min = 0, max = 9999)
    @XmlElement(name = "LocalRefundCount")
    private int localRefundCount;
    @Range(min = 0, max = 1000000)
    @XmlJavaTypeAdapter(value = BigDecimalAdapter.class)
    @XmlElement(name = "LocalRefund")
    private BigDecimal localRefund;
    @Range(min = 0, max = 1000000)
    @XmlJavaTypeAdapter(value = BigDecimalAdapter.class)
    @XmlElement(name = "LocalSales")
    private BigDecimal localSales;
    @Range(min = 0, max = 9999)
    @XmlElement(name = "OnlineSalesCount")
    private int onlineSalesCount;
    @Range(min = 0, max = 9999)
    @XmlElement(name = "OnlineRefundCount")
    private int onlineRefundCount;
    @Range(min = 0, max = 1000000)
    @XmlJavaTypeAdapter(value = BigDecimalAdapter.class)
    @XmlElement(name = "OnlineRefund")
    private BigDecimal onlineRefund;
    @Range(min = 0, max = 1000000)
    @XmlJavaTypeAdapter(value = BigDecimalAdapter.class)
    @XmlElement(name = "OnlineSales")
    private BigDecimal onlineSales;
    @Range(min = 0, max = 9999)
    @XmlElement(name = "PastSaleCount")
    private int pastSaleCount;
    @Range(min = 0, max = 1000000)
    @XmlJavaTypeAdapter(value = BigDecimalAdapter.class)
    @XmlElement(name = "PastSales")
    private BigDecimal pastSales;

    //    public String getScreenCode() {
    //        return StringUtils.trimLeadingCharacter(this.screenCode.substring(0, this.screenCode.length() - 1), '0')
    //                + this.screenCode.substring(this.screenCode.length() - 1);
    //    }
}
