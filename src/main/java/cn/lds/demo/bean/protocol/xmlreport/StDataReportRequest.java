package cn.lds.demo.bean.protocol.xmlreport;


import cn.lds.demo.bean.protocol.Payload;
import cn.lds.demo.bean.protocol.enums.PacketSource;
import cn.lds.demo.bean.protocol.enums.PacketType;
import cn.lds.demo.bean.protocol.format.DateTimeAdapter;
import com.leadingsoft.bizfuse.common.web.utils.json.JsonUtils;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@Getter
@Setter
@XmlRootElement(name = "Data")
@XmlAccessorType(XmlAccessType.NONE)
public class StDataReportRequest implements Payload {

    private static final long serialVersionUID = 7019435849579058749L;

    private PacketSource packetSource = PacketSource.CINEMA;

    /**
     * 原始的xml报文内容
     */
    private String originalXml;

    @Override
    public PacketType getPacketType() {
        return PacketType.STDATAREPORT;
    }

    @Override
    public String getDiscription() {
        return "【A5】【影院2中心】统计上报";
    }

    @Override
    public PacketSource getPacketSource() {
        return this.packetSource;
    }

    @XmlJavaTypeAdapter(value = DateTimeAdapter.class)
    @XmlAttribute(name = "Datetime")
    private Date datetime;

    @Length(max = 8, min = 8)
    @XmlAttribute(name = "DestinationCode")
    private String destinationCode;

    @Length(max = 8, min = 8)
    @XmlAttribute(name = "SourceCode")
    private String sourceCode;

    @XmlAttribute(name = "Type")
    private String type;

    @XmlAttribute(name = "Version")
    private String version;

    @XmlElement(name = "TicketReport")
    private TicketReport ticketReport;

    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private DsSignature dsSignature;

    @Override
    public String toString() {

        return JsonUtils.pojoToJson(this);
    }
}
