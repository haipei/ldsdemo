package cn.lds.demo.bean.protocol.format;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期格式化 "yyyy-MM-dd'T'HH:mm:ss"
 */
public class DateTimeAdapter extends XmlAdapter<String, Date> {

    private final String pattern = "yyyy-MM-dd'T'HH:mm:ss";
    SimpleDateFormat fmt = new SimpleDateFormat(pattern);

    @Override
    public synchronized Date unmarshal(final String dateStr) throws Exception {
        return fmt.parse(dateStr);
    }

    @Override
    public synchronized String marshal(final Date date) throws Exception {
        return fmt.format(date);
    }
}
