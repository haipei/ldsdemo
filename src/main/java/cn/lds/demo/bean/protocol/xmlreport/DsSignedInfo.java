package cn.lds.demo.bean.protocol.xmlreport;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@XmlRootElement(name = "SignedInfo", namespace = "http://www.w3.org/2000/09/xmldsig#")
@XmlAccessorType(XmlAccessType.NONE)
public class DsSignedInfo {
    @XmlElement(name = "CanonicalizationMethod", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private DsCanonicalizationMethod dsCanonicalizationMethod;

    @XmlElement(name = "SignatureMethod", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private DsSignatureMethod dsSignatureMethod;

    @XmlElement(name = "Reference", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private DsReference dsReference;
}
