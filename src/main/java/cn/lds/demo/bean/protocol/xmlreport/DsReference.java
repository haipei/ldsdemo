package cn.lds.demo.bean.protocol.xmlreport;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@XmlRootElement(name = "Reference", namespace = "http://www.w3.org/2000/09/xmldsig#")
@XmlAccessorType(XmlAccessType.NONE)
public class DsReference {
    @XmlAttribute(name = "URI")
    private String uri;

    @XmlElement(name = "DigestMethod", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private DsDigestMethod digestMethod;

    @XmlElement(name = "DigestValue", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private String digestValue;
}
