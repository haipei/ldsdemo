package cn.lds.demo.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.web.util.UriUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.Calendar;
import java.util.Date;

@Slf4j
public class RsaKeyUtils {

    public static final String SIGN_ALGORITHMS = "SHA1WithRSA";

    public static String sign(final String content, final PrivateKey priKey) {
        try {
            final byte[] md5 = DigestUtils.md5(content);

            final java.security.Signature signature = java.security.Signature.getInstance(RsaKeyUtils.SIGN_ALGORITHMS);
            signature.initSign(priKey);
            signature.update(md5);
            final byte[] signed = signature.sign();
            return Base64.encodeBase64String(signed);
        } catch (final Exception e) {
            RsaKeyUtils.log.error("计算签名失败。", e);
        }
        return null;
    }

    public static boolean verify(final String content, final String sign, final PublicKey pubKey) {
        try {
            final byte[] md5 = DigestUtils.md5(content);

            final java.security.Signature signature = java.security.Signature
                    .getInstance(RsaKeyUtils.SIGN_ALGORITHMS);

            signature.initVerify(pubKey);
            signature.update(md5);

            final boolean bverify = signature.verify(Base64.decodeBase64(sign));
            if(log.isDebugEnabled()){
                log.debug("校验签名结果：{}",bverify);
            }
            return bverify;
        } catch (final Exception e) {
            log.error("校验签名失败，原因是：\n{}", ExceptionUtils.getFullStackTrace(e));
        }

        return false;
    }

    public static KeyPair loadKeyPair(final String rsaKeyFileRoot, final String clientNo,
                                      final String keyStorePassword, final String privateKeyPassword) {
        try {
            if(log.isDebugEnabled()){

                RsaKeyUtils.log.error("rsaKeyFileRoot:" + rsaKeyFileRoot);
            }
            final KeyStore ks = KeyStore.getInstance("PKCS12");
            final File file = new File(rsaKeyFileRoot + "/" + clientNo + ".pfx");
            final byte[] clientPfx = IOUtils.toByteArray(new FileInputStream(file));
            ks.load(new ByteArrayInputStream(clientPfx),
                    keyStorePassword.toCharArray());

            final PrivateKey privateKey =
                    (PrivateKey) ks.getKey("1", privateKeyPassword.toCharArray());

            final Certificate certificate = ks.getCertificate("1");
            final PublicKey publicKey = certificate.getPublicKey();

            final KeyPair keyPair = new KeyPair(publicKey, privateKey);
            return keyPair;
        } catch (final Exception e) {
            RsaKeyUtils.log.error("加载客户端【{}】证书失败,原因如下：\n ", clientNo, ExceptionUtils.getFullStackTrace(e));
            return null;
        }
    }

    public static String buildSignStr(final String clientNo, final String cipherCode,
                                      final String serialNumber, final Date timestamp, final String nonce) {

        final StringBuffer sb = new StringBuffer();
        sb.append(clientNo).append("#");
        sb.append(cipherCode).append("#");
        sb.append(serialNumber).append("#");
        sb.append(BusinessDateUtils.formatOfBusinessTime(timestamp)).append("#");
        sb.append(nonce);

        return sb.toString();
    }

    public static String buildSignStr(final String clientNo, final String authKey, final Date timestamp,
                                      final String nonce) {
        final StringBuffer sb = new StringBuffer();
        sb.append(clientNo).append("#");
        sb.append(authKey).append("#");
        sb.append(BusinessDateUtils.formatOfBusinessTime(timestamp)).append("#");
        sb.append(nonce);

        return sb.toString();
    }

    public static String buildSignStr(final String clientNo, final String authKey, final String dataFilePath,
                                      final String nonce) {
        final StringBuffer sb = new StringBuffer();
        sb.append(clientNo).append("#");
        sb.append(authKey).append("#");
        sb.append(dataFilePath).append("#");
        sb.append(nonce);

        return sb.toString();
    }

    public static void main(final String[] argv) throws UnsupportedEncodingException {
        final String clientNo = "vodt";
        final String authKey = "VODT@2017";

        final KeyPair keyPair = RsaKeyUtils.loadKeyPair("D:/tmp", clientNo, "12345678", "12345678");

        final Calendar cd = Calendar.getInstance();
        cd.set(Calendar.YEAR, 2017);
        cd.set(Calendar.MONTH, 6);
        cd.set(Calendar.DAY_OF_MONTH, 25);
        cd.set(Calendar.HOUR_OF_DAY, 0);
        cd.set(Calendar.MINUTE, 0);
        cd.set(Calendar.SECOND, 0);
        final Date timestamp = cd.getTime();
        final String nonce = String.valueOf(RandomUtils.nextLong());

        {
            final String signatureStr = RsaKeyUtils.buildSignStr(clientNo, authKey, timestamp, nonce);
            final String signature = RsaKeyUtils.sign(signatureStr, keyPair.getPrivate());

            final StringBuffer sb = new StringBuffer();
            sb.append("http://59.252.101.3:10040/data/service/query/");
            sb.append(clientNo).append("/");
            sb.append(BusinessDateUtils.formatOfBusinessTime(timestamp)).append("/");
            sb.append(nonce).append("?");
            sb.append("signature=").append(UriUtils.encode(signature, "UTF-8"));
        }

        {
            final String dataFilePath = "data/201707/25/cinemasIncrement2017-07-25.050000.019.json.zip";
            final String signatureStr = RsaKeyUtils.buildSignStr(clientNo, authKey, dataFilePath, nonce);
            final String signature = RsaKeyUtils.sign(signatureStr, keyPair.getPrivate());


            final StringBuffer sb = new StringBuffer();
            sb.append("http://59.252.101.3:10040/data/service/download/");
            sb.append(clientNo).append("/");
            sb.append(nonce).append("?");
            sb.append("dataFilePath=").append(UriUtils.encode(dataFilePath, "UTF-8"));
            sb.append("&signature=").append(UriUtils.encode(signature, "UTF-8"));
        }

    }

}
