package cn.lds.demo.utils;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * 电影营业日工具类
 */
public final class BusinessDateUtils {

    public static final String DateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss";
    public static String BusinessDateFormat = "yyyy-MM-dd";

    public static Date parseBusinessDate(final String businessDate) {
        try {
            return DateUtils.parseDate(businessDate, Locale.CHINA, BusinessDateUtils.BusinessDateFormat);
        } catch (final ParseException e) {
            throw new RuntimeException("营业日格式错误.", e);
        }
    }

    public static Date parseBusinessTime(final String dateTime) {
        try {
            return DateUtils.parseDate(dateTime, Locale.CHINA, BusinessDateUtils.DateTimeFormat);
        } catch (final ParseException e) {
            throw new RuntimeException("营业日格式错误.", e);
        }
    }

    public static String formatOfBusinessTime(final Date dateTime) {
        return DateFormatUtils.format(dateTime, BusinessDateUtils.DateTimeFormat, Locale.CHINA);
    }

    public static String formatOfBusinessDate(final Date date) {
        return DateFormatUtils.format(date, BusinessDateUtils.BusinessDateFormat, Locale.CHINA);
    }

    /**
     * 获取当前营业日
     *
     * @return
     */
    public static Date getCurrentBusinessDate() {
        return BusinessDateUtils.convertToBusinessDate(new Date());
    }

    /**
     * 获取某一时刻的营业日
     *
     * @return
     */
    public static Date convertToBusinessDate(final Date dateTime) {
        final GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(dateTime);
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        if (hour < 6) {
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static String getCurrentBusinessDateStr() {
        final Date date = BusinessDateUtils.getCurrentBusinessDate();
        return BusinessDateUtils.formatOfBusinessDate(date);
    }

    /**
     * 获取前一营业日
     *
     * @return
     */
    public static Date getPreviousBusinessDate() {
        final GregorianCalendar calendar = new GregorianCalendar();
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        if (hour < 6) {
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
        }
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
