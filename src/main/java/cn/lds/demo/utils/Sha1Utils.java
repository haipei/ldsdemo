package cn.lds.demo.utils;

import org.springframework.util.Base64Utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Sha1Utils {

    public static byte[] SHA1Byte(final String inStr) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1"); //选择SHA-1，也可以选择MD5
            final byte[] digest = md.digest(inStr.getBytes()); //返回的是byet[]，要转化为String存储比较方便
            return digest;
        } catch (final NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
        }
        return "".getBytes();
    }

    public static String SHA1Base64(final String inStr) {
        MessageDigest md = null;
        String outStr = null;
        try {
            md = MessageDigest.getInstance("SHA-1"); //选择SHA-1，也可以选择MD5
            final byte[] digest = md.digest(inStr.getBytes()); //返回的是byet[]，要转化为String存储比较方便
            outStr = new String(Base64Utils.encode(digest));
        } catch (final NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
        }
        return outStr;
    }
}
