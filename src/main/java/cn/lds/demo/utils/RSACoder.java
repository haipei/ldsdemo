package cn.lds.demo.utils;

import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

/**
 * RSA编解码工具类
 *
 * @author liuyg
 */
@Slf4j
public class RSACoder {

    /**
     * 加密<br>
     * 用私钥加密
     *
     * @param data 数据
     * @param key 私钥
     * @return
     * @throws Exception
     */
    public static byte[] encryptByPrivateKey(final byte[] data, final PrivateKey privateKey)
            throws Exception {
        // 对数据加密
        final Cipher cipher = Cipher.getInstance(privateKey.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        return cipher.doFinal(data);
    }

    /**
     * 解密<br>
     * 用公钥解密
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] decryptByPublicKey(final byte[] data, final PublicKey key)
            throws Exception {
        // 对数据解密
        final Cipher cipher = Cipher.getInstance(key.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, key);

        return cipher.doFinal(data);
    }

    public static PrivateKey getPrivateKey(final String filePath, final String keyalias, final String keypasswd) {
        try {
            final KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(new FileInputStream(filePath), keypasswd.toCharArray());
            final PrivateKey privateKey = (PrivateKey) ks.getKey(keyalias, keypasswd.toCharArray());
            return privateKey;
        } catch (final Exception e) {
            RSACoder.log.error(e.getMessage(), e);
            return null;
        }
    }

    public static PrivateKey getPrivateKey(final InputStream stream, final String keyalias, final String keypasswd) {
        try {
            final KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(stream, keypasswd.toCharArray());
            final PrivateKey privateKey = (PrivateKey) ks.getKey(keyalias, keypasswd.toCharArray());
            return privateKey;
        } catch (final Exception e) {
            RSACoder.log.error(e.getMessage(), e);
            return null;
        }
    }

    public static PublicKey getPublicKey(final String filePath, final String keyalias, final String keypasswd) {
        try {
            return RSACoder.getPublicKey(new FileInputStream(filePath), keyalias, keypasswd);
        } catch (final Exception e) {
            RSACoder.log.error(e.getMessage(), e);
            return null;
        }
    }

    public static PublicKey getPublicKey(final InputStream stream, final String keyalias, final String keypasswd) {
        try {
            final KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(stream, keypasswd.toCharArray());
            final Certificate cert = ks.getCertificate(keyalias);
            final PublicKey pubkey = cert.getPublicKey();
            return pubkey;
        } catch (final Exception e) {
            RSACoder.log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 根据modulus和exponent值生成公钥
     *
     * @param modulus 十六进制值的字符
     * @param exponent 十六进制值的字符
     * @return
     */
    public static RSAPublicKey getPublicKeyFromStr(final String modulus, final String exponent) {
        final BigInteger m = new BigInteger(modulus, 16);
        final BigInteger e = new BigInteger(exponent, 16);
        try {
            final RSAPublicKeySpec spec = new RSAPublicKeySpec(m, e);
            final KeyFactory factory = KeyFactory.getInstance("RSA");
            final RSAPublicKey pub = (RSAPublicKey) factory.generatePublic(spec);
            return pub;
        } catch (final Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }

    /**
     * 根据modulus和exponent值生成私钥
     *
     * @param modulus 十六进制值的字符
     * @param exponent 十六进制值的字符
     * @return
     */
    public static PrivateKey getPrivateKeyFromStr(final String modulus, final String exponent) {
        final BigInteger m = new BigInteger(modulus, 16);
        final BigInteger e = new BigInteger(exponent, 16);
        try {
            final RSAPrivateKeySpec spec = new RSAPrivateKeySpec(m, e);
            final KeyFactory factory = KeyFactory.getInstance("RSA");
            final PrivateKey pri = factory.generatePrivate(spec);
            return pri;
        } catch (final Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }

    /**
     * 根据modulus和exponent值生成十六进制值的字符
     *
     * @param privateKey privateKey
     * @return
     */
    public static String convertPrivateKeyToStr(final RSAPrivateKey privateKey) {
        return privateKey.getModulus().toString(16) + "|" + privateKey.getPrivateExponent().toString(16);
    }

    //    public static void main(final String[] args) throws Exception {
    //        final String value =
    //                "B06C1D75A15E5AD2FFAE8C57CA4C8CBB7892A6A13A7BADA590A1BA7D7F8A3A17E1A36FD917F497392D5E25E8C1DDF3763BD46B9C52DC24E3B6FCCA6760E49E7F0EDD4A7B0CE9F89F86A3516301E0AE831BB2525E6BAB1B5B33A1B2AFFEA427244DF3DD06373BD3DBF00F9A000074DFFBE9D2C9FB5C43B855F321566BC0EF7569|010001";
    //        final String[] k = value.split("\\|");
    //        final RSAPublicKey key = RSACoder.getPublicKeyFromStr(k[0], k[1]);
    //        System.out.println(key);
    //    }

    //    public static void main(final String[] args) throws Exception {
    //        final PrivateKey privateKey = RSACoder.getPrivateKey("/home/liuyg/mtms-ca/11010101.pfx", "1", "12345678");
    //        final PublicKey publicKey = RSACoder.getPublicKey("/home/liuyg/mtms-ca/11010101.pfx", "1", "12345678");
    //
    //        final Payload1C body = new Payload1C();
    //        body.setCode("1234567");
    //        body.setCinema_status((byte) 1);
    //        final ByteBuf outBuff = Unpooled.buffer();
    //        ProtocolBeanUtils.encodeProtocolBean(body, outBuff);
    //        final byte[] bodyBytes = new byte[outBuff.readableBytes() - 128];
    //        outBuff.readBytes(bodyBytes);
    //        // 计算MD5
    //        final byte[] md5 = DigestUtils.md5(bodyBytes);
    //        // 私钥加密
    //        final byte[] encrypted = RSACoder.encryptByPrivateKey(md5, privateKey);
    //        System.out.println(encrypted.length == 128);
    //        // 公钥解密
    //        final byte[] decrypted = RSACoder.decryptByPublicKey(encrypted, publicKey);
    //        if (md5.length != decrypted.length) {
    //            System.out.println("解密后与原MD5不符");
    //            return;
    //        }
    //        for (int i = 0; i < md5.length; i++) {
    //            if (md5[i] != decrypted[i]) {
    //                System.out.println("解密后与原MD5不符");
    //                return;
    //            }
    //        }
    //        System.out.println("解密成功！");
    //    }
}
