package cn.lds.demo.utils;

import org.apache.xml.security.c14n.Canonicalizer;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class ValidataXMLUtil {

    public static String canonXml(final String xml) throws Exception {

        org.apache.xml.security.Init.init();

        final DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();

        dfactory.setNamespaceAware(true);
        dfactory.setValidating(true);

        final DocumentBuilder documentBuilder = dfactory.newDocumentBuilder();

        // this is to throw away all validation warnings
        documentBuilder.setErrorHandler(new org.apache.xml.security.utils.IgnoreAllErrorHandler());

        final byte inputBytes[] = xml.getBytes();
        final Document doc =
                documentBuilder.parse(new ByteArrayInputStream(inputBytes));

        // after playing around, we have our document now
        final Canonicalizer c14n = Canonicalizer.getInstance(
                "http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments");
        final byte outputBytes[] = c14n.canonicalizeSubtree(doc);

        return new String(outputBytes);

    }

    static String input = "<TicketReport Status=\"Past\" Id=\"ID_TicketReport\"><Cinema C" +
            "ode=\"12020031\" Status=\"Normal\"><Session><BusinessDate>2014-06-10</BusinessDate><ScreenCode>4620859962489359</ScreenCode><FilmCode>00"
            +
            "1101772014</FilmCode><SessionCode>S201406100001</SessionCode><SessionDatetime>2014-06-10T13:05:00</SessionDatetime><LocalSalesCount>"
            +
            "2</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalRefund>0.00</LocalRefund><LocalSales>60.00</LocalSales><OnlineSalesCo"
            +
            "unt>0</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineRefund>0.00</OnlineRefund><OnlineSales>0.00</OnlineSales><Pas"
            +
            "tSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session></Cinema></TicketReport>";

    public static void main(final String[] args) throws Exception {

        final String canonXml = ValidataXMLUtil.canonXml(ValidataXMLUtil.input);
        System.out.println(canonXml);
        System.out.println(Sha1Utils.SHA1Base64(canonXml));
        System.out.println(Sha1Utils.SHA1Base64(ValidataXMLUtil.input));
        //	O3UVlaIhmJL9VcbhwSJ/4d70B4s=
    }
}
