package cn.lds.demo.statistic;

import cn.lds.demo.bean.protocol.xmlreport.StDataReportRequest;
import cn.lds.demo.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.IOUtils;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.util.Base64Utils;
import org.xml.sax.*;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.sax.SAXSource;
import java.io.*;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPublicKey;

@Slf4j
public class StatisticReportRSATest {
    public static void main(final String[] args) throws Exception {
        String pfxPath = "D:\\tmp\\44101361.pfx";
        // 从文件中读取电文
        String xmlMessage = StatisticReportRSATest.readXml("D:\\tmp\\report.xml");
        // 获取验签公钥
        final PublicKey publicKey = StatisticReportRSATest.getLocalPublicKey(pfxPath);
       /* final String m = "AE6D019BB3A8DE9DCE8A1FC5F703E39E48B8351E91D2D62E5D97D044AEA2D88357CD34A06D500F1918764754E7451CACA1089970453FC15187AC7B73D87C8B56549E77F10F57F955E29CF4B6D639799411389B68895554A0F85D998E16B18A3A3526CA4787FFB6ACD94DFF043D51DC69AF3394187412EF1021BC45598980490F";
        final String e = "010001";
        final RSAPublicKey publicKey = CerUtils.getRSAPublicKeyFromStr(m, e);
        System.out.println(publicKey.getModulus().toString(16).toUpperCase().equals(m));
        System.out.println(publicKey.getPublicExponent().toString(16));*/

        //获取影院私钥
        PrivateKey privateKey = RSACoder.getPrivateKey(pfxPath, "1", "12345678");
//        PrivateKey privateKey = CerUtils.getPvkformPfx(pfxPath, "12345678");

        // 解析电文
        final StDataReportRequest statisticReport = StatisticReportRSATest.parseStDataReportRequest(xmlMessage);

        final String dsDigestValue =
                statisticReport.getDsSignature().getDsSignedInfo().getDsReference().getDigestValue();
        final String dsSignatureValue = statisticReport.getDsSignature().getDsSignatureValue();

        xmlMessage = ValidataXMLUtil.canonXml(xmlMessage);
        //数据证书校验
        final String subXml = xmlMessage.substring(xmlMessage.indexOf("<TicketReport"),
                xmlMessage.indexOf("</TicketReport>") + 15);

        final String shaMIBase64 = Sha1Utils.SHA1Base64(subXml);
        if ((shaMIBase64 == null) || (dsDigestValue == null)
                || !shaMIBase64.equals(dsDigestValue)) {
            //sha校验不通过
            StatisticReportRSATest.log.error("[{}]统计上报签名sha1校验失败 ", "aa");
            return;
        }

        //签名校验
        final byte[] sha1Byte = Sha1Utils.SHA1Byte(subXml.trim());
        boolean rsaMi = false;
        final Signature signature = Signature.getInstance("NONEwithRSA");
        signature.initVerify(publicKey);
        signature.update(sha1Byte);
        rsaMi = signature.verify(Base64Utils.decode(dsSignatureValue.replaceAll("\\s", "").getBytes()));


        byte[] dsSignature = null;
        try {
            dsSignature = Base64Utils.decode(dsSignatureValue.getBytes());
        } catch (final Exception ex) {// 如果base64有换行空白，替换后执行解码
//            CinemaStaticReportDecoder.log.warn("[{}]Base64签名解码失败，尝试去空白后重试， 签名值:{}", clientNo, dsSignatureValue);
            dsSignature = Base64Utils.decode(dsSignatureValue.replaceAll("\\s", "").getBytes());
        }
        rsaMi = signature.verify(dsSignature);
        if (!rsaMi) {//签名校验 rsa不通过
            final String errorMsg = String.format("[%s]统计上报RSA签名[%s]校验失败", 11, dsSignature);
            StatisticReportRSATest.log.error(errorMsg);
        }
        if (!rsaMi) {
            //签名校验 rsa不通过
            StatisticReportRSATest.log.error("[{}]统计上报签名rsa校验失败 ", "bb");
            return;
        }
        StatisticReportRSATest.log.info("正常报文");
        //私钥签名
        signature.initSign(privateKey);
        signature.update(sha1Byte);
        StatisticReportRSATest.log.info(Base64Utils.encodeToString(signature.sign()));

    }

    // 获取数据源
    private static DataSource dataSource() {
        return DataSourceBuilder.create().driverClassName("com.mysql.jdbc.Driver")
                .url("jdbc:mysql://172.17.11.135:3306/bits_db?useUnicode=true&characterEncoding=utf-8&useSSL=false&autoReconnect=true")
                .password("password@bits")
                .username("bits")
                .build();
    }

    // 从本地文件读取
    private static PublicKey getLocalPublicKey(final String path) throws FileNotFoundException {
        final File cert = new File(path);
        return RSACoder.getPublicKey(new FileInputStream(cert), "1", "12345678");
    }

    // 从数据库读取

    //    public static PublicKey getDatabasePublicKey(final String cinemaCode)
    //            throws SQLException {
    //        final String sql =
    //                "select file_contents from certificate_file where file_name like ?";
    //        final JdbcTemplate template = new JdbcTemplate(StatisticReportRSATest.dataSource());
    //        template.afterPropertiesSet();
    //        final List<byte[]> values =
    //                template.query(sql, (PreparedStatementSetter) ps -> ps.setString(1,
    //                        "reportcer@1@mainKey@" + cinemaCode + "@%.pfx"), (RowMapper<byte[]>) (rs,
    //                                rowNum) -> rs.getBytes(1));
    //        final byte[] certBytes = values.get(0);
    //        final PublicKey publicKey = RSACoder.getPublicKey(new ByteArrayInputStream(certBytes), "1", "12345678");
    //        return publicKey;
    //    }

    private static StDataReportRequest parseStDataReportRequest(final String xmlMessage)
            throws SAXException, SAXNotRecognizedException, SAXNotSupportedException, JAXBException {
        final InputSource inputSource = new InputSource(new StringReader(xmlMessage));
        final XMLReader xmlReader = XMLReaderFactory.createXMLReader();
        xmlReader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        final String featureName = "http://xml.org/sax/features/external-general-entities";
        xmlReader.setFeature(featureName, false);
        xmlReader.setEntityResolver((publicId, systemId) -> new InputSource(new StringReader("")));

        final SAXSource saxSource = new SAXSource(xmlReader, inputSource);

        final Unmarshaller unmarshaller = JAXBContext.newInstance(StDataReportRequest.class).createUnmarshaller();

        final StDataReportRequest statisticReport = (StDataReportRequest) unmarshaller.unmarshal(saxSource);
        statisticReport.setOriginalXml(xmlMessage);
        return statisticReport;
    }

    private static String readXml(final String path) throws IOException {
        final BufferedInputStream in = new BufferedInputStream(new FileInputStream(path));
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(in, out);
        return out.toString("UTF-8");
    }

}
