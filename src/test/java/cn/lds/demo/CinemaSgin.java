package cn.lds.demo;

import cn.lds.demo.utils.CerUtils;
import cn.lds.demo.utils.Sha1Utils;
import cn.lds.demo.utils.ValidataXMLUtil;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.springframework.util.Base64Utils;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPublicKey;

/**
 * 影院签名
 */
public class CinemaSgin {
    public static void main(String[] args) {
        final String m = "E42FA6CB6F9BA3A1761A0940B2AE592977C711393981BC4CA241FDC2AE8395F140EABE14A1C5C5876AA13134AACB1119879533EA32B9DD100352358EF511D5B8C8B42E8A535B88F50DF0ABE085F179313254DA3B53CF8C6C28919CEE9C1EDE009B06B68E661A907AD0255799DAB630729BC260B7C5C1C3C4774E103ADA8FC2B7";
        final String e = "010001";
        final RSAPublicKey pubKey = CerUtils.getRSAPublicKeyFromStr(m, e);
        //签名校验
        //获取16进制sha1 值进行解签
        //      String shaMI=Sha1Utils.SHA1(subXml.trim());
        final byte[] sha1Byte = org.apache.commons.codec.binary.Base64.decodeBase64("sJa6549FkepEIRszhTTxR0PXwfw=");
        boolean rsaMi = false;
        try {
            final Signature signature = Signature.getInstance("NONEwithRSA");
            signature.initVerify(pubKey);
            signature.update(sha1Byte);
            rsaMi = signature.verify(org.apache.commons.codec.binary.Base64.decodeBase64(("n9Ch4c1+agScBonpbWQypubDK24IL3FrRPdZ+EuGIUI+FQWwOmSAYZJj8B43GhADrXZ9JnXfuteVpN5RiavecskUbPw0+inETwbrz1Do2IOP+G0xIfsK5lhszWApaU8A0mJuAskXgdoG0F5e1tbVS7Yt/24OJ5C189NS+lKF5zo=")));
            System.out.println(rsaMi);
        } catch (final Exception e3) {
            e3.printStackTrace();
        }
    }
    @Test
    public void des1() throws Exception {
        final String m = "E42FA6CB6F9BA3A1761A0940B2AE592977C711393981BC4CA241FDC2AE8395F140EABE14A1C5C5876AA13134AACB1119879533EA32B9DD100352358EF511D5B8C8B42E8A535B88F50DF0ABE085F179313254DA3B53CF8C6C28919CEE9C1EDE009B06B68E661A907AD0255799DAB630729BC260B7C5C1C3C4774E103ADA8FC2B7";
        final String e = "010001";
        final RSAPublicKey pubKey = CerUtils.getRSAPublicKeyFromStr(m, e);
        //签名校验
        //获取16进制sha1 值进行解签
        //      String shaMI=Sha1Utils.SHA1(subXml.trim());
        final byte[] sha1Byte = org.apache.commons.codec.binary.Base64.decodeBase64("sJa6549FkepEIRszhTTxR0PXwfw=");
        boolean rsaMi = false;
        try {
            final Signature signature = Signature.getInstance("NONEwithRSA");
            signature.initVerify(pubKey);
            signature.update(sha1Byte);
            rsaMi = signature.verify(org.apache.commons.codec.binary.Base64.decodeBase64(("n9Ch4c1+agScBonpbWQypubDK24IL3FrRPdZ+EuGIUI+FQWwOmSAYZJj8B43GhADrXZ9JnXfuteVpN5RiavecskUbPw0+inETwbrz1Do2IOP+G0xIfsK5lhszWApaU8A0mJuAskXgdoG0F5e1tbVS7Yt/24OJ5C189NS+lKF5zo=")));
            System.out.println(rsaMi);
        } catch (final Exception e3) {
            e3.printStackTrace();
        }
    }
    @Test
    public void des() throws Exception {
        final String m = "E74305C5B075D724BF62DC3B50434A8B0061A1EFC3EA0AA73FC52BE63E4A3B9B993EDA04AB0892664E41EF423D6B16A24478858736A2B501E79F0ABCD534ED64BF03830E719EC602391DCD14B31899930DD53BB2CD349438997910631383EC8789516FBE35FA2FC646F12E3D4C1977A5B50C237C36A285BA26EEE97BDE01463F";
        final String e = "010001";
        final RSAPublicKey pubKey = CerUtils.getRSAPublicKeyFromStr(m, e);
        PublicKey publicKey = CerUtils.getPukformPfx("D:\\git\\ldsdemo\\src\\test\\resources\\reportcer@2@1@13055401@20161227033758.pfx", "12345678");
        //签名校验
        //获取16进制sha1 值进行解签
        //      String shaMI=Sha1Utils.SHA1(subXml.trim());
        final byte[] sha1Byte = org.apache.commons.codec.binary.Base64.decodeBase64("0H2gFc/esNR+QhOkSTMTwMktySs=");
        boolean rsaMi = false;
        try {
            final Signature signature = Signature.getInstance("NONEwithRSA");
            signature.initVerify(pubKey);
            signature.update(sha1Byte);


            rsaMi = signature.verify(org.apache.commons.codec.binary.Base64.decodeBase64((signStr)));

            System.out.println(rsaMi);
        } catch (final Exception e3) {
            e3.printStackTrace();
        }
    }
    @Test
    public void signTest() {

        try {
            //jR1IWXlrhmHVV5VJ3EHA34N6aOJR8RF+HYrY8vJgZQdM8Te549MDYk0Cjfkoc+12X9EKcUy83EPZ
            final String m = "E74305C5B075D724BF62DC3B50434A8B0061A1EFC3EA0AA73FC52BE63E4A3B9B993EDA04AB0892664E41EF423D6B16A24478858736A2B501E79F0ABCD534ED64BF03830E719EC602391DCD14B31899930DD53BB2CD349438997910631383EC8789516FBE35FA2FC646F12E3D4C1977A5B50C237C36A285BA26EEE97BDE01463F";
            final String e = "010001";
            final RSAPublicKey pubKey = CerUtils.getRSAPublicKeyFromStr(m, e);
            PrivateKey privateKey = CerUtils.getPvkformPfx("D:\\git\\ldsdemo\\src\\test\\resources\\reportcer@2@1@13055401@20161227033758.pfx", "12345678");
            PublicKey publicKey = CerUtils.getPukformPfx("D:\\git\\ldsdemo\\src\\test\\resources\\reportcer@2@1@13055401@20161227033758.pfx", "12345678");
            System.out.println(((RSAPublicKey) publicKey).getModulus());
            String subxml = xmlstr.substring(xmlstr.indexOf("<TicketReport"), xmlstr.indexOf("</TicketReport>") + 15);
            subxml = ValidataXMLUtil.canonXml(subxml);

            final java.security.Signature signature = java.security.Signature.getInstance("NONEwithRSA");
            signature.initSign(privateKey);
            signature.update(Sha1Utils.SHA1Base64(subxml.trim()).getBytes());
            final byte[] signed = signature.sign();
            String s = org.apache.commons.codec.binary.Base64.encodeBase64String(signed);
            System.out.println(Sha1Utils.SHA1Base64(subxml.trim()));
            System.out.println(s);

            Signature design = Signature.getInstance("NONEwithRSA");
            design.initVerify(publicKey);
            design.update(Sha1Utils.SHA1Base64(subxml.trim()).getBytes());
            System.out.println(design.verify(org.apache.commons.codec.binary.Base64.decodeBase64(s)));

            Signature design_1 = Signature.getInstance("NONEwithRSA");
            design_1.initVerify(pubKey);
            design_1.update(Sha1Utils.SHA1Base64(subxml.trim()).getBytes());
            System.out.println(design_1.verify(org.apache.commons.codec.binary.Base64.decodeBase64(s)));

            Signature design_2 = Signature.getInstance("NONEwithRSA");
            design_2.initVerify(pubKey);
            design_2.update(Sha1Utils.SHA1Base64(subxml.trim()).getBytes());
            System.out.println(design.verify(cn.lds.demo.utils.Base64.decode(s)));

        } catch (Exception e) {
        }
    }

    String signStr = "jR1IWXlrhmHVV5VJ3EHA34N6aOJR8RF+HYrY8vJgZQdM8Te549MDYk0Cjfkoc+12X9EKcUy83EPZRDyQUBGJH52dar5QGqAsbpRTpmjhaRTE4hEAJZd0j0Wjvg9qhEVPRAqdimTA2XoOG5lFnCJ23YsSMIlFWFMK/s19+gQdong=";

    String xmlstr2 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Data Version=\"2.0\" Type=\"TicketReport\" Datetime=\"2018-07-19T11:45:03\" SourceCode=\"31012401\" DestinationCode=\"00000000\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://www.crifst.ac.cn/2013/TicketReport.xsd\"><TicketReport Status=\"Past\" Id=\"ID_TicketReport\"><Cinema Code=\"31012401\" Status=\"Test\"><Session><BusinessDate>2018-07-01</BusinessDate> <ScreenCode>0000000000000000</ScreenCode><FilmCode>000000000000</FilmCode><SessionCode>0000000000000000</SessionCode><SessionDatetime>2018-07-01T06:00:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalRefund>0.00</LocalRefund><LocalSales>0.00</LocalSales><OnlineSalesCount>0</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineRefund>0.00</OnlineRefund><OnlineSales>0.00</OnlineSales><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session></Cinema></TicketReport><ds:Signature xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments\"/><ds:SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><ds:Reference URI=\"#ID_TicketReport\"><ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><ds:DigestValue>sJa6549FkepEIRszhTTxR0PXwfw=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>humiRudf8QoOY++lCMj6hwJFij9anfOShgCOaLVDynzLQ7073fuFXC2W4nOiHZconyc+zLwtQkN4f1Iqjk5dWXNqDTNr/vt/uE7JMyMPhn/cuuvE7Kr3VIiCDHfTUTkrk6yZnRUe97AqsJvGObYbtjZyDUgt7Bzbg6GEi7xb2BY=</ds:SignatureValue></ds:Signature></Data>";

    String xmlstr = "<xml>" +
            "traceIdTRACE:n4.tcs.IN.118071900001530730</traceId>" +
            "<clientIp>111.61.0.6</clientIp>" +
            "<x-ssl-client-cert-dn>/C=CN/ST=BEIJING/L=BEIJING/O=13055401/CN=13055401</x-ssl-client-cert-dn>" +
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            "<Data xmlns:noNamespaceSchemaLocation=\"http://www.crifst.ac.cn/2013/TicketReport.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:ds=\"http://ww" +
            "w.w3.org/2000/09/xmldsig#\" Version=\"2.0\" Type=\"TicketReport\" Datetime=\"2018-07-19T08:06:11\" SourceCode=\"13055401\" DestinationCode=\"00000000\"><TicketReport Stat" +
            "us=\"Past\" Id=\"ID_TicketReport\"><Cinema Code=\"13055401\" Status=\"Normal\"><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000001</ScreenCode" +
            "><FilmCode>001104142017</FilmCode><SessionCode>0000000000009159</SessionCode><SessionDatetime>2017-09-03T17:10:00</SessionDatetime><LocalSalesCount>1</LocalSal" +
            "esCount><LocalRefundCount>0</LocalRefundCount><LocalSales>25.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>2</OnlineSalesCount><OnlineRefundC" +
            "ount>0</OnlineRefundCount><OnlineSales>54.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session" +
            "><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000002</ScreenCode><FilmCode>074101492017</FilmCode><SessionCode>0000000000009180</Sessi" +
            "onCode><SessionDatetime>2017-09-03T21:20:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalS" +
            "ales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>2</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>60.00</OnlineSales><OnlineRefund" +
            ">0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000" +
            "000002</ScreenCode><FilmCode>074101492017</FilmCode><SessionCode>0000000000008876</SessionCode><SessionDatetime>2017-09-03T19:25:00</SessionDatetime><LocalSale" +
            "sCount>1</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>25.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>4</OnlineSalesCo" +
            "unt><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>108.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</" +
            "PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000002</ScreenCode><FilmCode>074101492017</FilmCode><SessionCode>0000" +
            "000000008875</SessionCode><SessionDatetime>2017-09-03T17:25:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><Loca" +
            "lSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>7</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>189.00</Onlin" +
            "eSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><S" +
            "creenCode>0000000000000002</ScreenCode><FilmCode>074101492017</FilmCode><SessionCode>0000000000008874</SessionCode><SessionDatetime>2017-09-03T15:25:00</Sessio" +
            "nDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCou" +
            "nt>7</OnlineSalesCount><OnlineRefundCount>2</OnlineRefundCount><OnlineSales>201.00</OnlineSales><OnlineRefund>54.00</OnlineRefund><PastSaleCount>0</PastSaleCou" +
            "nt><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000002</ScreenCode><FilmCode>012101512017</FilmCo" +
            "de><SessionCode>0000000000008873</SessionCode><SessionDatetime>2017-09-03T13:05:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</Lo" +
            "calRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>12</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><Onli" +
            "neSales>339.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-0" +
            "9-03</BusinessDate><ScreenCode>0000000000000002</ScreenCode><FilmCode>074101492017</FilmCode><SessionCode>0000000000009163</SessionCode><SessionDatetime>2017-0" +
            "9-03T11:05:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalR" +
            "efund><OnlineSalesCount>1</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>30.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleC" +
            "ount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000002</ScreenCode><FilmCode>0" +
            "74101492017</FilmCode><SessionCode>0000000000009162</SessionCode><SessionDatetime>2017-09-03T09:05:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><Loc" +
            "alRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>2</OnlineSalesCount><OnlineRefundCount>0</Onlin" +
            "eRefundCount><OnlineSales>57.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><Bu" +
            "sinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000003</ScreenCode><FilmCode>012101512017</FilmCode><SessionCode>0000000000009179</SessionCode><Sessi" +
            "onDatetime>2017-09-03T19:05:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRe" +
            "fund>0.00</LocalRefund><OnlineSalesCount>5</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>138.00</OnlineSales><OnlineRefund>0.00</Onlin" +
            "eRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000003</Scre" +
            "enCode><FilmCode>012101512017</FilmCode><SessionCode>0000000000008879</SessionCode><SessionDatetime>2017-09-03T16:45:00</SessionDatetime><LocalSalesCount>0</Lo" +
            "calSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>12</OnlineSalesCount><OnlineR" +
            "efundCount>0</OnlineRefundCount><OnlineSales>333.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></" +
            "Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000003</ScreenCode><FilmCode>012101512017</FilmCode><SessionCode>0000000000009164" +
            "</SessionCode><SessionDatetime>2017-09-03T09:40:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00<" +
            "/LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>0</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>0.00</OnlineSales><Online" +
            "Refund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000" +
            "000000000004</ScreenCode><FilmCode>001105662017</FilmCode><SessionCode>0000000000009178</SessionCode><SessionDatetime>2017-09-03T16:40:00</SessionDatetime><Loc" +
            "alSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>8</OnlineSa" +
            "lesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>222.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0" +
            ".00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000004</ScreenCode><FilmCode>001105662017</FilmCode><SessionCode" +
            ">0000000000009177</SessionCode><SessionDatetime>2017-09-03T22:20:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount>" +
            "<LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>0</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>0.00</On" +
            "lineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate" +
            "><ScreenCode>0000000000000004</ScreenCode><FilmCode>001105662017</FilmCode><SessionCode>0000000000008884</SessionCode><SessionDatetime>2017-09-03T18:30:00</Ses" +
            "sionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSales" +
            "Count>0</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>0.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCou" +
            "nt><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000004</ScreenCode><FilmCode>001104142017</FilmCo" +
            "de><SessionCode>0000000000008882</SessionCode><SessionDatetime>2017-09-03T14:45:00</SessionDatetime><LocalSalesCount>3</LocalSalesCount><LocalRefundCount>0</Lo" +
            "calRefundCount><LocalSales>75.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>6</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><Onli" +
            "neSales>168.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-0" +
            "9-03</BusinessDate><ScreenCode>0000000000000004</ScreenCode><FilmCode>001105662017</FilmCode><SessionCode>0000000000008881</SessionCode><SessionDatetime>2017-0" +
            "9-03T12:55:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalR" +
            "efund><OnlineSalesCount>1</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>27.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleC" +
            "ount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000004</ScreenCode><FilmCode>0" +
            "74101492017</FilmCode><SessionCode>0000000000009176</SessionCode><SessionDatetime>2017-09-03T20:20:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><Loc" +
            "alRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>2</OnlineSalesCount><OnlineRefundCount>0</Onlin" +
            "eRefundCount><OnlineSales>54.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><Bu" +
            "sinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000004</ScreenCode><FilmCode>001105662017</FilmCode><SessionCode>0000000000009168</SessionCode><Sessi" +
            "onDatetime>2017-09-03T09:15:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRe" +
            "fund>0.00</LocalRefund><OnlineSalesCount>0</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>0.00</OnlineSales><OnlineRefund>0.00</OnlineR" +
            "efund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000004</Screen" +
            "Code><FilmCode>001104142017</FilmCode><SessionCode>0000000000009167</SessionCode><SessionDatetime>2017-09-03T11:05:00</SessionDatetime><LocalSalesCount>0</Loca" +
            "lSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>2</OnlineSalesCount><OnlineRefu" +
            "ndCount>0</OnlineRefundCount><OnlineSales>54.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Sess" +
            "ion><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000005</ScreenCode><FilmCode>012101512017</FilmCode><SessionCode>0000000000009175</Se" +
            "ssionCode><SessionDatetime>2017-09-03T20:50:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</Loc" +
            "alSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>0</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>0.00</OnlineSales><OnlineRefu" +
            "nd>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>00000000" +
            "00000005</ScreenCode><FilmCode>074101492017</FilmCode><SessionCode>0000000000009174</SessionCode><SessionDatetime>2017-09-03T18:50:00</SessionDatetime><LocalSa" +
            "lesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>4</OnlineSalesC" +
            "ount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>114.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00<" +
            "/PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000005</ScreenCode><FilmCode>074101492017</FilmCode><SessionCode>000" +
            "0000000009170</SessionCode><SessionDatetime>2017-09-03T14:35:00</SessionDatetime><LocalSalesCount>1</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><Loc" +
            "alSales>25.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>12</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>324.00</On" +
            "lineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate" +
            "><ScreenCode>0000000000000001</ScreenCode><FilmCode>001904512017</FilmCode><SessionCode>0000000000009161</SessionCode><SessionDatetime>2017-09-03T21:15:00</Ses" +
            "sionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSales" +
            "Count>2</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>60.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCo" +
            "unt><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000001</ScreenCode><FilmCode>001904512017</FilmC" +
            "ode><SessionCode>0000000000009160</SessionCode><SessionDatetime>2017-09-03T19:00:00</SessionDatetime><LocalSalesCount>1</LocalSalesCount><LocalRefundCount>0</L" +
            "ocalRefundCount><LocalSales>30.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>4</OnlineSalesCount><OnlineRefundCount>2</OnlineRefundCount><Onl" +
            "ineSales>120.00</OnlineSales><OnlineRefund>60.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017" +
            "-09-03</BusinessDate><ScreenCode>0000000000000001</ScreenCode><FilmCode>051201402017</FilmCode><SessionCode>0000000000009158</SessionCode><SessionDatetime>2017" +
            "-09-03T15:15:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>2</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>50.00</Loc" +
            "alRefund><OnlineSalesCount>3</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>84.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSa" +
            "leCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000001</ScreenCode><FilmCod" +
            "e>051201402017</FilmCode><SessionCode>0000000000009157</SessionCode><SessionDatetime>2017-09-03T13:20:00</SessionDatetime><LocalSalesCount>2</LocalSalesCount><" +
            "LocalRefundCount>0</LocalRefundCount><LocalSales>50.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>5</OnlineSalesCount><OnlineRefundCount>0</O" +
            "nlineRefundCount><OnlineSales>141.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Sessio" +
            "n><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000001</ScreenCode><FilmCode>001904512017</FilmCode><SessionCode>0000000000009156</SessionCode><" +
            "SessionDatetime>2017-09-03T11:05:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><Lo" +
            "calRefund>0.00</LocalRefund><OnlineSalesCount>2</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>60.00</OnlineSales><OnlineRefund>0.00</O" +
            "nlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000001</" +
            "ScreenCode><FilmCode>051201402017</FilmCode><SessionCode>0000000000009155</SessionCode><SessionDatetime>2017-09-03T09:10:00</SessionDatetime><LocalSalesCount>1" +
            "</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>30.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>5</OnlineSalesCount><Onl" +
            "ineRefundCount>0</OnlineRefundCount><OnlineSales>135.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSale" +
            "s></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000003</ScreenCode><FilmCode>012101512017</FilmCode><SessionCode>000000000000" +
            "8878</SessionCode><SessionDatetime>2017-09-03T14:25:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0" +
            ".00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>6</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>165.00</OnlineSales><" +
            "OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCod" +
            "e>0000000000000003</ScreenCode><FilmCode>012101512017</FilmCode><SessionCode>0000000000009166</SessionCode><SessionDatetime>2017-09-03T21:25:00</SessionDatetim" +
            "e><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>2</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>50.00</LocalRefund><OnlineSalesCount>0</O" +
            "nlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>0.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSa" +
            "les>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000003</ScreenCode><FilmCode>001904512017</FilmCode><Sessio" +
            "nCode>0000000000009165</SessionCode><SessionDatetime>2017-09-03T12:05:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundC" +
            "ount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>2</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>60." +
            "00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</Busine" +
            "ssDate><ScreenCode>0000000000000005</ScreenCode><FilmCode>001904512017</FilmCode><SessionCode>0000000000009173</SessionCode><SessionDatetime>2017-09-03T16:35:0" +
            "0</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00</LocalRefund><Onlin" +
            "eSalesCount>19</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>570.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><PastSaleCount>0</Pa" +
            "stSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDate>2017-09-03</BusinessDate><ScreenCode>0000000000000005</ScreenCode><FilmCode>07520135201" +
            "7</FilmCode><SessionCode>0000000000009172</SessionCode><SessionDatetime>2017-09-03T12:00:00</SessionDatetime><LocalSalesCount>1</LocalSalesCount><LocalRefundCo" +
            "unt>0</LocalRefundCount><LocalSales>30.00</LocalSales><LocalRefund>0.00</LocalRefund><OnlineSalesCount>0</OnlineSalesCount><OnlineRefundCount>3</OnlineRefundCo" +
            "unt><OnlineSales>0.00</OnlineSales><OnlineRefund>90.00</OnlineRefund><PastSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session><Session><BusinessDat" +
            "e>2017-09-03</BusinessDate><ScreenCode>0000000000000005</ScreenCode><FilmCode>075201352017</FilmCode><SessionCode>0000000000009171</SessionCode><SessionDatetim" +
            "e>2017-09-03T09:25:00</SessionDatetime><LocalSalesCount>0</LocalSalesCount><LocalRefundCount>0</LocalRefundCount><LocalSales>0.00</LocalSales><LocalRefund>0.00" +
            "</LocalRefund><OnlineSalesCount>0</OnlineSalesCount><OnlineRefundCount>0</OnlineRefundCount><OnlineSales>0.00</OnlineSales><OnlineRefund>0.00</OnlineRefund><Pa" +
            "stSaleCount>0</PastSaleCount><PastSales>0.00</PastSales></Session></Cinema></TicketReport><ds:Signature><ds:SignedInfo><ds:CanonicalizationMethod Algorithm=\"ht" +
            "tp://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments\"/><ds:SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><ds:Reference URI=\"#ID" +
            "_TicketReport\"><ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><ds:DigestValue>0H2gFc/esNR+QhOkSTMTwMktySs=</ds:DigestValue></ds:Reference" +
            "></ds:SignedInfo><ds:SignatureValue>jR1IWXlrhmHVV5VJ3EHA34N6aOJR8RF+HYrY8vJgZQdM8Te549MDYk0Cjfkoc+12X9EKcUy83EPZ" +
            "RDyQUBGJH52dar5QGqAsbpRTpmjhaRTE4hEAJZd0j0Wjvg9qhEVPRAqdimTA2XoOG5lFnCJ23YsS" +
            "MIlFWFMK/s19+gQdong=</ds:SignatureValue></ds:Signature></Data>";
}
