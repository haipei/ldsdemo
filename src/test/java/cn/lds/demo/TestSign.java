package cn.lds.demo;


import org.apache.commons.codec.binary.Base64;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.Signature;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;

public class TestSign {
    /**
     * 根据modulus和exponent值生成公钥
     * @param modulus  十六进制值的字符
     * @param exponent 十六进制值的字符
     * @return
     */
    public static RSAPublicKey getRSAPublicKeyFromStr(String modulus,String exponent ){
        BigInteger m=	new BigInteger(modulus,16);
        BigInteger e=	new BigInteger(exponent,16);
        try {
            RSAPublicKeySpec spec = new RSAPublicKeySpec(m,e);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            RSAPublicKey pub = (RSAPublicKey)factory.generatePublic(spec);
            return pub;
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }


    public static void main(String[] args){

        final String m = "E42FA6CB6F9BA3A1761A0940B2AE592977C711393981BC4CA241FDC2AE8395F140EABE14A1C5C5876AA13134AACB1119879533EA32B9DD100352358EF511D5B8C8B42E8A535B88F50DF0ABE085F179313254DA3B53CF8C6C28919CEE9C1EDE009B06B68E661A907AD0255799DAB630729BC260B7C5C1C3C4774E103ADA8FC2B7";
        final String e = "010001";
        final RSAPublicKey pubKey = getRSAPublicKeyFromStr(m, e);
        //DigestValue 报文中的字段
        final byte[] sha1Byte = org.apache.commons.codec.binary.Base64.decodeBase64("sJa6549FkepEIRszhTTxR0PXwfw=");
        boolean rsaMi = false;
        try {
            final Signature signature = Signature.getInstance("NONEwithRSA");
            signature.initVerify(pubKey);
            signature.update(sha1Byte);
            rsaMi = signature.verify(Base64.decodeBase64(("n9Ch4c1+agScBonpbWQypubDK24IL3FrRPdZ+EuGIUI+FQWwOmSAYZJj8B43GhADrXZ9JnXfuteVpN5RiavecskUbPw0+inETwbrz1Do2IOP+G0xIfsK5lhszWApaU8A0mJuAskXgdoG0F5e1tbVS7Yt/24OJ5C189NS+lKF5zo=")));

            System.out.println(rsaMi);


        } catch (GeneralSecurityException ex) {

        }
    }

}
